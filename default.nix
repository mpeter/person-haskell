# Nix derivation of the backend package
# use `cabal2nix . > cabal.nix` to update dependencies from cabal

{ pkgs ? import ../pinned-nixpkgs.nix {}, ... }:
pkgs.lib.overrideDerivation (pkgs.haskell.packages.ghc802.callPackage ./cabal.nix {}) (old : {
  doCheck = false;
})

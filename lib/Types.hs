-- |
-- Module: Types
--
-- This module contains the core data model for the app.  It is implemented
-- using Haskell records.
module Types (
    CreateReq (CR)
  , UpdateReq (UR)
  , migrateAll
  , module Types.Misc
  , module Types.Person
  , module Types.Widget
  ) where

import           Types.CreateReq (CreateReq (CR))
import           Types.Misc
import           Types.Person
import           Types.TH
import           Types.UpdateReq (UpdateReq (UR))
import           Types.Widget

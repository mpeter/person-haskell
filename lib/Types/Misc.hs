{-# LANGUAGE FlexibleInstances    #-}
{-# LANGUAGE OverloadedStrings    #-}
{-# LANGUAGE TypeSynonymInstances #-}
module Types.Misc (
  Date
  , DateTime
  , Text
  , UUID
  , Color
  , HasUniqueId(..)
  , Attribute (..)
  , Document'
  , modifier
  ) where

import           Control.Lens
import           Data.Char            (toLower)
import           Data.Swagger.JSONApi (Attribute (..))
import qualified Data.Text            as T
import           Data.Time
import           Network.JSONApi      (Document, ResourcefulEntity (..))

instance ResourcefulEntity (Attribute Int) where
  resourceIdentifier = T.pack . show . value
  resourceType _ = "Int"
  resourceLinks _ = Nothing
  resourceMetaData _ = Nothing
  resourceRelationships _ = Nothing

-- | Each system user (including users in the database) have a unique ID
type UUID = String

instance ResourcefulEntity (Attribute String) where
  resourceIdentifier = T.pack . value
  resourceType _ = "String"
  resourceLinks _ = Nothing
  resourceMetaData _ = Nothing
  resourceRelationships _ = Nothing

-- | For now we use the 'String' type for textual data
type Text = String

-- | Colors are represented by their hex code
type Color = String

-- | We use UTC dates
type Date = Day

type DateTime = UTCTime

instance ResourcefulEntity (Attribute UTCTime) where
  resourceIdentifier = T.pack . show . value
  resourceType _ = "DateTime"
  resourceLinks _ = Nothing
  resourceMetaData _ = Nothing
  resourceRelationships _ = Nothing

-- | The class of types that have a unique id field
class HasUniqueId a where
  uniqueIdL :: Lens' a UUID

-- | This is used to derive ToJSON/FromJSON instances
modifier :: Int -> String -> String
modifier n = go . drop n
  where
    go []     = []
    go (x:xs) = toLower x : xs

-- | A type synonym for simple values
type Document' a = Document (Attribute a)

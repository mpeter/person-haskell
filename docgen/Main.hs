{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE Trustworthy       #-}

module Main where

import           Data.Aeson                 (encode)
import qualified Data.ByteString.Lazy.Char8 as BSL8
import           Docs

-- | Generate the documentation
main = BSL8.writeFile "swagger.json" $ encode swaggerDoc

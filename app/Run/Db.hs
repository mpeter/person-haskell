{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE TypeFamilies          #-}

-- |
-- Module: Run.Db
--
-- This module collects functions for constructing a database session
module Run.Db (
  initDb
  , RunDb
  , ConnectionString
  , connString
  ) where

import           Control.Monad.IO.Class      (MonadIO (..))
import           Control.Monad.Logger        (MonadLogger, runStdoutLoggingT)
import           Control.Monad.Reader        (ReaderT (..))
import           Control.Monad.Trans.Control (MonadBaseControl)
import           Data.ByteString             (append)
import           Data.Monoid                 ((<>))
import           Database.Persist.Postgresql (ConnectionString, runMigration)
import           Database.Persist.Sql        (SqlBackend, runSqlPool)
import           System.Posix.Env.ByteString (getEnvDefault)
import           Types                       (migrateAll)

-- | This general polymorphic type represents functions that can run database operations
type RunDb b = forall m a. (MonadBaseControl IO m, MonadLogger m, MonadIO m) => ReaderT b m a -> m a

-- | Script to set up the database
initDb :: (MonadIO m, MonadLogger m) =>
  (forall a. ReaderT SqlBackend m a -> m a) -> m ()
initDb withConn =
    withConn $
      runMigration migrateAll

-- | Form a connection string from the environment
connString :: MonadIO m => m ConnectionString
connString = liftIO $ do
  db   <- getEnvDefault "PGDATABASE" "main"
  port <- getEnvDefault "PGPORT" "8081"
  return $ "dbname=" <> db <> " port=" <> port <> " host=localhost"

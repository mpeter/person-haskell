{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE Trustworthy       #-}

module Run.Error (
  AppError (..)
  , toServantErr
  , upgradeError
  ) where

import           Data.Aeson                 (encode)
import           Data.ByteString.Lazy       (append, toStrict)
import           Data.ByteString.Lazy.Char8 (pack, unpack)
import qualified Data.Text                  as T
import           Data.Text.Encoding         (decodeUtf8)
import           Network.HTTP.Types.Header  (hContentType)
import           Network.JSONApi            (Error (..), ErrorDocument (..))
import           Network.Wai                (responseStatus)
import           Servant.Server             (ServantErr (..), err400, err404,
                                             err500)

--
-- Add better error handling to the Wai application
--

upgradeError :: ServantErr -> ServantErr
upgradeError = toJsonApi

--
-- App errors
--

-- | Error type to represent exceptional situations
data AppError =
  NoResult
  | InvalidField String
  | MissingHeader String
  | MissingParam String
  | AppError String

-- | Transform a domain error into the more general 'ServantErr'
toServantErr :: AppError -> ServantErr
toServantErr NoResult = err404 { errBody = "No matching results." }
toServantErr (InvalidField f) = err400 { errBody = pack $ "Field " ++ f ++ " malformed." }
toServantErr (MissingHeader h) = err400 { errBody = pack $ "Header " ++ h ++ " is required." }
toServantErr (MissingParam p) = err400 { errBody = pack $ "Query parameter " ++ p ++ " required." }
toServantErr (AppError m) = err500 { errBody = pack m }

-- | Add the header "content-type: application/json"
addJsonCTH :: ServantErr -> ServantErr
addJsonCTH e = e { errHeaders = h' } where
  h' = (hContentType, "application/json") : errHeaders e

-- | Given a 'ServantErr' where the body is a plain string, repackage it as a JSON API error document
toJsonApi :: ServantErr -> ServantErr
toJsonApi err =
  err { errHeaders = [(hContentType, "application/vnd.json+api")], errBody = encode $ err' } where
    err' = ErrorDocument japierr Nothing Nothing :: ErrorDocument ()
    japierr =
      Error
        Nothing
        Nothing
        (Just $ T.pack $ show $ errHTTPCode err)
        Nothing
        Nothing
        -- Can throw an impure error if non unicode data is found in the error body
        (Just $ decodeUtf8 $ toStrict $ errBody err)
        Nothing

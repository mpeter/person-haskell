{-# LANGUAGE CPP             #-}
{-# LANGUAGE TemplateHaskell #-}
-- |
-- Run.Redis
--
-- Instances for MonadRedis
module Run.Redis where

import           Control.Monad.Logger       (LoggingT)
import           Control.Monad.Reader       (ReaderT (..))
import           Control.Monad.Trans        (lift)
import           Control.Monad.Trans.Except (ExceptT (..))
import           Database.Redis             (MonadRedis (..), Redis)

-- Note that although we are using the C preprocessor to generate these
-- instances, it is trivial to write them by hand

#define REDIS(M) \
instance MonadRedis m => MonadRedis (M m) where \
  liftRedis = lift. liftRedis

REDIS(ReaderT b)
REDIS(ExceptT b)
REDIS(LoggingT)

#undef REDIS

{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE Trustworthy           #-}

module Servant.API.ContentTypes.JSONApi (
  JSONApi
  , MimeRender (..)
  , MimeUnrender (..)
  ) where

import           Data.Aeson         (FromJSON, ToJSON, eitherDecode, encode)
import           Network.HTTP.Media ((//))
import           Network.JSONApi
import           Servant.API

-- | Type used to reflect JSONApi encoding details
data JSONApi

instance Accept JSONApi where
  contentType _ = "application" // "vnd.api+json"

-- | This implementation does not support links or meta fields
instance ToJSON a => MimeRender JSONApi (Document a) where
  mimeRender _ = encode

instance FromJSON a => MimeUnrender JSONApi (Document a) where
  mimeUnrender _ = eitherDecode

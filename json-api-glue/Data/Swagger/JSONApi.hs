{-# LANGUAGE DefaultSignatures    #-}
{-# LANGUAGE DeriveGeneric        #-}
{-# LANGUAGE FlexibleContexts     #-}
{-# LANGUAGE OverloadedLists      #-}
{-# LANGUAGE OverloadedStrings    #-}
{-# LANGUAGE ScopedTypeVariables  #-}
{-# LANGUAGE StandaloneDeriving   #-}
{-# LANGUAGE TypeFamilies         #-}
{-# LANGUAGE UndecidableInstances #-}
-- |
-- Module: Data.Swagger.JSONApi
--
-- Swagger schema for JSONApi
module Data.Swagger.JSONApi (
  Attribute (..)
  ) where

import           Control.Lens    ((&), (.~))
import           Data.Aeson      (FromJSON, ToJSON)
import           Data.Proxy
import           Data.Swagger    (NamedSchema (..), SwaggerType (..),
                                  ToSchema (..), declareSchemaRef, properties,
                                  required, type_)
import           Data.Text       (Text)
import           GHC.Generics    (Generic)
import           Network.JSONApi

-- | Document gets an unnamed schema with only its data field
instance (ToSchema a) => ToSchema (Document a) where
  declareNamedSchema _ = do
    subSch <- declareSchemaRef (Proxy :: Proxy (Resource a))
    return $ NamedSchema Nothing $ mempty
      & type_ .~ SwaggerObject
      & properties .~
        [ ("data", subSch) ]
      & required .~
        [ "data" ]

-- | Resource gets an unnamed schema with its id, type, and attributes fields
instance (ToSchema a) => ToSchema (Resource a) where
  declareNamedSchema _ = do
    subSch <- declareSchemaRef (Proxy :: Proxy a)
    text <- declareSchemaRef (Proxy :: Proxy Text)
    return $ NamedSchema Nothing $ mempty
      & type_ .~ SwaggerObject
      & properties .~
        [ ("id", text)
        , ("type", text)
        , ("attributes", subSch) ]
      & required .~
        [ "id", "type"]

-- | We need to wrap values so that we can meet the constraint that the value
-- of the attributes field of a resource object is always an object.
newtype Attribute a = Attribute { value :: a }
  deriving (Eq, Show, Generic)

instance ToJSON a => ToJSON (Attribute a)
instance FromJSON a => FromJSON (Attribute a)

-- Straightforward implementation (unnamed schema)
instance ToSchema a => ToSchema (Attribute a) where
  declareNamedSchema _ = do
    subSch <- declareSchemaRef (Proxy :: Proxy a)
    return $ NamedSchema Nothing $ mempty
      & type_ .~ SwaggerObject
      & properties .~
        [ ("value", subSch) ]
      & required .~
        [ "value" ]

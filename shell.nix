# Nix shell environment for backend development

{ pkgs ? import ../pinned-nixpkgs.nix {}, ... }:
let
  update-deps = pkgs.writeScriptBin "update-deps" ''
    #!${pkgs.stdenv.shell}
    ${pkgs.cabal2nix}/bin/cabal2nix . > cabal.nix
  '';
in pkgs.lib.overrideDerivation (import ./default.nix { inherit pkgs; }).env (old : {
  name = "macguffin-backend-shell";
  buildInputs = old.buildInputs ++ [ update-deps ];
})

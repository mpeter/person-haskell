{ mkDerivation, aeson, base, bytestring, cereal, esqueleto, hedis
, hspec, http-client, http-media, lens, mockery, monad-logger, mtl
, persistent, persistent-postgresql, persistent-sqlite
, persistent-template, random, servant, servant-client
, servant-server, servant-swagger, servant-swagger-ui, stdenv
, swagger2, text, time, transformers, uuid, wai, warp
}:
mkDerivation {
  pname = "haskell-app";
  version = "0.1.2.1";
  src = ./.;
  isLibrary = true;
  isExecutable = true;
  libraryHaskellDepends = [
    aeson base http-media lens persistent persistent-template servant
    servant-swagger servant-swagger-ui swagger2 time
  ];
  executableHaskellDepends = [
    aeson base bytestring cereal esqueleto hedis lens monad-logger mtl
    persistent persistent-postgresql persistent-sqlite random servant
    servant-server servant-swagger-ui text time transformers uuid wai
    warp
  ];
  testHaskellDepends = [
    aeson base bytestring cereal esqueleto hedis hspec http-client lens
    mockery monad-logger mtl persistent persistent-postgresql
    persistent-sqlite persistent-template random servant servant-client
    servant-server servant-swagger servant-swagger-ui swagger2 text
    time transformers uuid wai warp
  ];
  license = stdenv.lib.licenses.mit;
}
